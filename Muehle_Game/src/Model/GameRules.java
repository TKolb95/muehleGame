package Model;

import container.Gameplay;
import controller.GameController.Players;
import javafx.scene.input.Dragboard;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;

/* Autor: Tobias Kolb
 * 3.Semester WI
 *
 */

public class GameRules
{
	// Alle Steine sind in diesem Array abgespeichert
	private  Gameplay[][] gameArray;
	
	public GameRules()
	{
		gameArray = new Gameplay[8][8];
		for(int i = 0 ; i < gameArray.length; i++)
		{
			for(int j = 0; j < gameArray.length; j++)
			{
				gameArray[i][j] = new Gameplay();
			}
		}
	}
	
	public Gameplay[][] getGameArray()
	{
		return this.gameArray;
	}
	
	private Gameplay fillArray(Circle lastStone, String stone)
	{
		Gameplay tempGameplay = new Gameplay(stone,lastStone);
		gameArray[tempGameplay.getX()][tempGameplay.getY()] = tempGameplay;
		return tempGameplay;
	}
	
	//lastStone = Spielstein, der auf ein Feld gesetzt wird 
	// example: blackcircle1
	// stone = ID im Feld
	// example: circle01, circle67
	public boolean checkMuehle(GridPane gridPaneGame, Circle lastStone, String stone)
	{
		System.out.println("Auf Feld: "  + stone + " wird der Stein: " + lastStone.getId() + " gesetzt");
		System.out.println("Farbe: " + getColor(lastStone));
		System.out.println("X: " + getX(stone));
		System.out.println("Y:" + getY(stone));
		System.out.println("-------------------");
		
		Gameplay tempGameplay = fillArray(lastStone,stone);
		
		switch(tempGameplay.getX())
		{
		case 0: 
			if(tempGameplay.getY() == 1)
			{
				if(tempGameplay.getColor().equals(gameArray[0][4].getColor()) && tempGameplay.getColor().equals(gameArray[0][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][4].setInMuehle(true);
					gameArray[0][7].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][1].getColor()) && tempGameplay.getColor().equals(gameArray[6][1].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][1].setInMuehle(true);
					gameArray[6][1].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 4)
			{
				if(tempGameplay.getColor().equals(gameArray[0][1].getColor()) && tempGameplay.getColor().equals(gameArray[0][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][1].setInMuehle(true);
					gameArray[0][7].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[1][4].getColor()) && tempGameplay.getColor().equals(gameArray[2][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][4].setInMuehle(true);
					gameArray[2][4].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 7)
			{
				if(tempGameplay.getColor().equals(gameArray[0][1].getColor()) && tempGameplay.getColor().equals(gameArray[0][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][1].setInMuehle(true);
					gameArray[0][4].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][7].getColor()) && tempGameplay.getColor().equals(gameArray[6][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][7].setInMuehle(true);
					gameArray[6][7].setInMuehle(true);
					return true;
				}
			}
			break;
		case 1:
 			if(tempGameplay.getY() == 2)
			{
				if(tempGameplay.getColor().equals(gameArray[3][2].getColor()) && tempGameplay.getColor().equals(gameArray[5][2].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][2].setInMuehle(true);
					gameArray[5][2].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[1][4].getColor()) && tempGameplay.getColor().equals(gameArray[1][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][4].setInMuehle(true);
					gameArray[1][6].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 4)
			{
				if(tempGameplay.getColor().equals(gameArray[0][4].getColor()) && tempGameplay.getColor().equals(gameArray[2][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][4].setInMuehle(true);
					gameArray[2][4].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[1][2].getColor()) && tempGameplay.getColor().equals(gameArray[1][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][2].setInMuehle(true);
					gameArray[1][6].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 6)
			{
				if(tempGameplay.getColor().equals(gameArray[3][6].getColor()) && tempGameplay.getColor().equals(gameArray[5][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][6].setInMuehle(true);
					gameArray[5][6].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[1][4].getColor()) && tempGameplay.getColor().equals(gameArray[1][2].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][4].setInMuehle(true);
					gameArray[1][2].setInMuehle(true);
					return true;
				}
			}
			break;
		case 2:
			if(tempGameplay.getY() == 3)
			{
				if(tempGameplay.getColor().equals(gameArray[3][3].getColor()) && tempGameplay.getColor().equals(gameArray[5][2].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][3].setInMuehle(true);
					gameArray[5][2].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[2][4].getColor()) && tempGameplay.getColor().equals(gameArray[2][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[2][4].setInMuehle(true);
					gameArray[2][5].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 4)
			{
				if(tempGameplay.getColor().equals(gameArray[2][3].getColor()) && tempGameplay.getColor().equals(gameArray[2][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[2][3].setInMuehle(true);
					gameArray[2][5].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[0][4].getColor()) && tempGameplay.getColor().equals(gameArray[1][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][4].setInMuehle(true);
					gameArray[1][4].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 5)
			{
				if(tempGameplay.getColor().equals(gameArray[3][5].getColor()) && tempGameplay.getColor().equals(gameArray[4][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][5].setInMuehle(true);
					gameArray[4][5].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[2][4].getColor()) && tempGameplay.getColor().equals(gameArray[2][3].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[2][4].setInMuehle(true);
					gameArray[2][3].setInMuehle(true);
					return true;
				}
			}
			break;
		case 3:
			if(tempGameplay.getY() == 1)
			{
				if(tempGameplay.getColor().equals(gameArray[0][1].getColor()) && tempGameplay.getColor().equals(gameArray[6][1].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][1].setInMuehle(true);
					gameArray[6][1].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][2].getColor()) && tempGameplay.getColor().equals(gameArray[3][3].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][2].setInMuehle(true);
					gameArray[3][3].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 2)
			{
				if(tempGameplay.getColor().equals(gameArray[1][2].getColor()) && tempGameplay.getColor().equals(gameArray[5][2].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][2].setInMuehle(true);
					gameArray[5][2].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][1].getColor()) && tempGameplay.getColor().equals(gameArray[3][3].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][1].setInMuehle(true);
					gameArray[3][3].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 3)
			{
				if(tempGameplay.getColor().equals(gameArray[2][3].getColor()) && tempGameplay.getColor().equals(gameArray[4][3].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[2][3].setInMuehle(true);
					gameArray[4][3].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][2].getColor()) && tempGameplay.getColor().equals(gameArray[3][1].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][2].setInMuehle(true);
					gameArray[3][1].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 5)
			{
				if(tempGameplay.getColor().equals(gameArray[2][5].getColor()) && tempGameplay.getColor().equals(gameArray[4][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[2][5].setInMuehle(true);
					gameArray[4][5].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][6].getColor()) && tempGameplay.getColor().equals(gameArray[3][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][7].setInMuehle(true);
					gameArray[3][6].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 6)
			{
				if(tempGameplay.getColor().equals(gameArray[3][5].getColor()) && tempGameplay.getColor().equals(gameArray[3][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][5].setInMuehle(true);
					gameArray[3][7].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[1][6].getColor()) && tempGameplay.getColor().equals(gameArray[5][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][6].setInMuehle(true);
					gameArray[5][6].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 7)
			{
				if(tempGameplay.getColor().equals(gameArray[0][7].getColor()) && tempGameplay.getColor().equals(gameArray[6][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][7].setInMuehle(true);
					gameArray[6][7].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[3][6].getColor()) && tempGameplay.getColor().equals(gameArray[3][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][6].setInMuehle(true);
					gameArray[3][5].setInMuehle(true);
					return true;
				}
			}
			break;
		case 4:
			if(tempGameplay.getY() == 3)
			{
				if(tempGameplay.getColor().equals(gameArray[3][3].getColor()) && tempGameplay.getColor().equals(gameArray[2][3].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[3][3].setInMuehle(true);
					gameArray[2][3].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[4][4].getColor()) && tempGameplay.getColor().equals(gameArray[4][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[4][4].setInMuehle(true);
					gameArray[4][5].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 4)
			{
				if(tempGameplay.getColor().equals(gameArray[4][3].getColor()) && tempGameplay.getColor().equals(gameArray[4][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[4][3].setInMuehle(true);
					gameArray[4][5].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[5][4].getColor()) && tempGameplay.getColor().equals(gameArray[6][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[5][4].setInMuehle(true);
					gameArray[6][4].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 5)
			{
				if(tempGameplay.getColor().equals(gameArray[2][5].getColor()) && tempGameplay.getColor().equals(gameArray[3][5].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[2][5].setInMuehle(true);
					gameArray[3][5].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[4][3].getColor()) && tempGameplay.getColor().equals(gameArray[4][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[4][3].setInMuehle(true);
					gameArray[4][4].setInMuehle(true);
					return true;
				}
			}
			break;
		case 5:
			if(tempGameplay.getY() == 2)
			{
				if(tempGameplay.getColor().equals(gameArray[5][4].getColor()) && tempGameplay.getColor().equals(gameArray[5][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[5][4].setInMuehle(true);
					gameArray[5][6].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[1][2].getColor()) && tempGameplay.getColor().equals(gameArray[3][2].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][2].setInMuehle(true);
					gameArray[3][2].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 4)
			{
				if(tempGameplay.getColor().equals(gameArray[4][4].getColor()) && tempGameplay.getColor().equals(gameArray[6][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[4][4].setInMuehle(true);
					gameArray[6][4].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[5][2].getColor()) && tempGameplay.getColor().equals(gameArray[5][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[5][2].setInMuehle(true);
					gameArray[5][6].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 6)
			{
				if(tempGameplay.getColor().equals(gameArray[1][6].getColor()) && tempGameplay.getColor().equals(gameArray[3][6].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[1][6].setInMuehle(true);
					gameArray[3][6].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[5][4].getColor()) && tempGameplay.getColor().equals(gameArray[5][2].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[5][4].setInMuehle(true);
					gameArray[5][2].setInMuehle(true);
					return true;
				}
			}
			break;
		case 6:
			if(tempGameplay.getY() == 1)
			{
				if(tempGameplay.getColor().equals(gameArray[0][1].getColor()) && tempGameplay.getColor().equals(gameArray[3][1].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][1].setInMuehle(true);
					gameArray[3][1].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[6][4].getColor()) && tempGameplay.getColor().equals(gameArray[6][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[6][4].setInMuehle(true);
					gameArray[6][7].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 4)
			{
				if(tempGameplay.getColor().equals(gameArray[4][4].getColor()) && tempGameplay.getColor().equals(gameArray[5][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[4][4].setInMuehle(true);
					gameArray[5][4].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[6][1].getColor()) && tempGameplay.getColor().equals(gameArray[6][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[6][1].setInMuehle(true);
					gameArray[6][7].setInMuehle(true);
					return true;
				}
			}
			if(tempGameplay.getY() == 7)
			{
				if(tempGameplay.getColor().equals(gameArray[0][7].getColor()) && tempGameplay.getColor().equals(gameArray[3][7].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[0][7].setInMuehle(true);
					gameArray[3][7].setInMuehle(true);
					return true;
				}
				else if(tempGameplay.getColor().equals(gameArray[6][1].getColor()) && tempGameplay.getColor().equals(gameArray[6][4].getColor()))
				{
					System.out.println("M�hle von " + tempGameplay.getColor());
					tempGameplay.setInMuehle(true);
					gameArray[6][1].setInMuehle(true);
					gameArray[6][4].setInMuehle(true);
					return true;
				}
			}
			break;
		}
		
		return false;
	}
	
	private String getColor(Circle lastStone)
	{
		if(lastStone != null)
		{
			return lastStone.getId().subSequence(0, 5).toString();
		}
		return null;
	}
	
	private char getX(String stone)
	{
		return stone.charAt(6);
	}
	
	private char getY(String stone)
	{
		return stone.charAt(7);
	}
}
