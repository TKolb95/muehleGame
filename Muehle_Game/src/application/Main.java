package application;
	
import java.io.IOException;

import container.Gameplay;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;


/* Autor: Tobias Kolb
 * 3.Semester WI
 * 
 * Das Projekt hat Spa� gemacht auch wenn ich es nicht geschafft habe alle Bugs zu entfernen
 * Speziell das Ziehen in Phase 2 der KI bereitet Probleme.
 * Leider habe ich keine L�sung gefunden das GridPane zu aktualisieren
 * 
 * Ein weiterer Ansatz w�re gewesen die komplette GridPane zu l�schen und neu
 * aufzumalen, allerdings m�sste hier einiges umgestellt werden und dies ist in der vorhandenen
 * Zeit nicht realisierbar.
 * 
 * Trotzdem freue ich mich auf Ihr Feedback
 * 
 */

public class Main extends Application 
{
	
	private static Stage stage;
	
	public void setStage(Stage stage)
	{
		this.stage = stage;
	}
	
	public static Stage getStage()
	{
		return stage;
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			Parent loader = FXMLLoader.load(getClass().getResource("/views/Game.fxml"));
			root.setCenter(loader);
			Scene scene = new Scene(root);
			setStage(primaryStage);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
