package container;

import javafx.scene.shape.Circle;

public class Gameplay 
{
	private int X;
	
	private int Y;
	
	private String color;
	
	private boolean inMuehle;
	
	private String stoneId;
	
	private Circle circle;
	
	// Default Konstruktor
	public Gameplay()
	{
		this.X = 0;
		this.Y = 0;
		this.color = "invisible";
		this.inMuehle = false;
		this.stoneId = "notDefined";
		this.circle = new Circle();
	}
	
	public Gameplay(String feldId, Circle stone)
	{
		this.X = Integer.parseInt(getX(feldId));
		this.Y = Integer.parseInt(getY(feldId));
		this.color = getColor(stone);
		this.inMuehle = false;
		this.stoneId = stone.getId();
		this.circle = stone;
	}
	
	private String getColor(Circle lastStone)
	{
		if(lastStone != null)
		{
			return lastStone.getId().subSequence(0, 5).toString();
		}
		return null;
	}
	
	public void setColor(String color)
	{
		this.color = color;
	}
	
	public void setY(int y)
	{
		this.Y = y;
	}
	
	public void setX(int x)
	{
		this.X = x;
	}
	
	private String getX(String stone)
	{
		return ""+stone.charAt(6);
	}
	
	private String getY(String stone)
	{
		return ""+stone.charAt(7);
	}
	

	public Circle getCircle()
	{
		return this.circle;
	}
	
	public void setCircle(Circle circle)
	{
		this.circle = circle;
	}
	
	public String getStoneId()
	{
		return this.stoneId;
	}
	
	public void setStoneId(String stoneId)
	{
		this.stoneId = stoneId;
	}
	
	public void setInMuehle(boolean option)
	{
		this.inMuehle = option;
	}
	
	public String getColor()
	{
		return this.color;
	}
	
	public boolean getInMuehle()
	{
		return this.inMuehle;
	}
	
	public int getX()
	{
		return this.X;
	}
	
	public int getY()
	{
		return this.Y;
	}
}
