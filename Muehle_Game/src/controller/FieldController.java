package controller;

import java.util.Optional;

import application.Main;
import container.Gameplay;
import controller.GameController.Phase;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/* Autor: Tobias Kolb
 * 3.Semester WI
 *
 */
public class FieldController
{
	
	public GridPane gridPaneGame;
	public GridPane gridPaneBlack;
	public GridPane gridPaneWhite;
	
	public FieldController(GridPane gridPaneBlack, GridPane gridPaneWhite, GridPane gridPaneGame)
	{
		this.gridPaneGame = gridPaneGame;
		this.gridPaneBlack = gridPaneBlack;
		this.gridPaneWhite = gridPaneWhite;
	}
	
	public FieldController(GridPane gridPaneGame)
	{
		this.gridPaneGame = gridPaneGame;
	}
	
	public void remove(GridPane gridPane, String id, Gameplay[][] gameArray)
	{
		for(Node n : gridPane.getChildren())
		{
			if(n.getId() != null)
			{
				if(n.getId().equals(id))
				{
					try
					{
						gridPane.getChildren().remove(n);
						break;
					}
					catch(Exception ex)
					{
						System.out.println("Exception: " + ex.toString());
					}
				}
			}
		}
	}
	

	public void removeStone(GridPane gridPaneGame, String id, Gameplay[][] gameArray)
	{
		for(int index = 0; index < gridPaneGame.getChildren().size();index++)
		{
			if(gridPaneGame.getChildren().get(index).getId() != null)
			{
				System.out.println("N: "+ gridPaneGame.getChildren().get(index).getId());
				if(gridPaneGame.getChildren().get(index).getId().equals(id))
				{
					try
					{
						// Array durchlaufen um diesen Stein zu finden und die ID des Feldes auszulesen
						for(int i= 0 ; i < gameArray.length; i++)
						{
							for(int j = 0; j < gameArray.length; j++)
							{
								if(gameArray[i][j] != null)
								{
									if(gameArray[i][j].getStoneId().equals(gridPaneGame.getChildren().get(index).getId()))
									{
										// Setze die ID des zu entfernden Steins auf die urspr�ngliche ID
										String tempId = "circle"+gameArray[i][j].getX()+""+gameArray[i][j].getY();
										gridPaneGame.getChildren().get(index).setId(tempId);
										gridPaneGame.getChildren().get(index).setOpacity(0);
										gameArray[i][j].setColor("invisible");
										gameArray[i][j].setStoneId(tempId);
										gameArray[i][j].setInMuehle(false);
										
										break;
									}
								}
							}
						}
						break;
					}
					catch(Exception ex)
					{
						System.out.println("Excpetion");
					}
				}
			}
		}
	}
	
	private String getX(String fieldId)
	{
		return ""+fieldId.charAt(6);
	}
	
	private String getY(String fieldId)
	{
		return ""+fieldId.charAt(7);
	}
	
	// circle == ID des Spielfeldes zb circl43
	// db == dieser Stein wird gesetzt
	// oldStoneId = blackCircle6
	public Gameplay replaceStone(DragEvent event, Circle circle, Gameplay[][] gameArray)
	{
		// replaceStone ist der Stein, welcher bewegt werden soll
		String oldStoneId = event.getDragboard().getString();
		
		Circle replaceStone = (Circle) event.getTarget();
		
		if(circle.getId().contains("circle"))
		{
			System.out.println("Hierhin kann gelegt werden");
			
			for(int i = 0; i < gameArray.length; i++)
			{
				for(int j = 0 ; j < gameArray.length; j++)
				{
					if(gameArray[i][j].getStoneId() != null)
					{
						if(gameArray[i][j].getStoneId().equals(oldStoneId))
						{
							// TODO FEHLER 
							// Stein wird aus dem Array gel�scht aber erscheint trotzdem auf dem Spielfeld
							removeStone(gridPaneGame,oldStoneId, gameArray);
							
							int x = Integer.parseInt(getX(circle.getId()));
							int y = Integer.parseInt(getY(circle.getId()));
							
							gameArray[x][y].setColor("black");
							gameArray[x][y].setStoneId(oldStoneId);
							gameArray[x][y].setCircle(replaceStone);
							gameArray[x][y].setInMuehle(false);
							gameArray[x][y].setX(x);
							gameArray[x][y].setY(y);
							
							setStone(event.getDragboard(),circle);
							
							// Kontrollausgabe
							System.out.println("Benutzer: Auf FeldId: " + replaceStone.getId() + " wurde der Stein: " + oldStoneId + " gesetzt");
							
							return gameArray[x][y];
						}
					}
					
				}
			}
			
			
		}
		return null;
	}
	
	// �berpr�fe den Spielzug des Spieler1 in Phase B
	
	// HINWEIS: Man k�nnte f�r jeden Stein eine Liste anlegen mit den g�ltigen Spielz�gen
	// W�rde den Code �bersichtlicher machen 
	
	public boolean checkPlayerTurn(DragEvent event, String game, GridPane gridPaneGame, Gameplay[][] gameArray)
	{
		// game = "blackCircle1
		System.out.println("ID Over: " + game);
		if(event != null && game != null && !(game.contains("white")) && !(game.contains("black")))
		{
			Circle oldStoneCircle = (Circle) event.getGestureSource();
			
			int newStoneIdX = Integer.parseInt(getX(game));
			int newStoneIdY = Integer.parseInt(getY(game));
			
			for(int i = 0; i < gameArray.length; i++)
			{
				for(int j = 0; j < gameArray.length; j++)
				{
					if(gameArray[i][j].getStoneId() != null && gameArray[i][j].getStoneId().equals(oldStoneCircle.getId()))
					{
						switch(gameArray[i][j].getX())
						{
						case 0: 
							if(gameArray[i][j].getY() == 1)
							{
								if(newStoneIdX == 0 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 1)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 4)
							{
								if(newStoneIdX == 0 && newStoneIdY == 7)
								{
									return true;
								}
								if(newStoneIdX == 0 && newStoneIdY == 1)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 7)
							{
								if(newStoneIdX == 0 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 7)
								{
									return true;
								}
								return false;
							}
							break;
						case 1:
							if(gameArray[i][j].getY() == 2)
							{
								if(newStoneIdX == 3 && newStoneIdY == 2)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 4)
							{
								if(newStoneIdX == 0 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 2 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 2)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 6)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 6)
							{
								if(newStoneIdX == 3 && newStoneIdY == 6)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							break;
						case 2:
							if(gameArray[i][j].getY() == 3)
							{
								if(newStoneIdX == 3 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 2 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 4)
							{
								if(newStoneIdX == 2 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 2 && newStoneIdY == 5)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 5)
							{
								if(newStoneIdX == 3 && newStoneIdY == 5)
								{
									return true;
								}
								if(newStoneIdX == 2 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							break;
						case 3:
							if(gameArray[i][j].getY() == 1)
							{
								if(newStoneIdX == 0 && newStoneIdY == 1)
								{
									return true;
								}
								if(newStoneIdX == 6 && newStoneIdY == 1)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 2)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 2)
							{
								if(newStoneIdX == 3 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 1)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 2)
								{
									return true;
								}
								if(newStoneIdX == 1 && newStoneIdY == 2)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 3)
							{
								if(newStoneIdX == 2 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 4 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 2)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 5)
							{
								if(newStoneIdX == 2 && newStoneIdY == 5)
								{
									return true;
								}
								if(newStoneIdX == 4 && newStoneIdY == 5)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 6)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 6)
							{
								if(newStoneIdX == 1 && newStoneIdY == 6)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 6)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 5)
								{
									return true;
								}
								if(newStoneIdX == 3 && newStoneIdY == 7)
								{
									return true;
								}
								return false;
							}
							break;
						case 4:
							if(gameArray[i][j].getY() == 3)
							{
								if(newStoneIdX == 3 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 4 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 4)
							{
								if(newStoneIdX == 4 && newStoneIdY == 3)
								{
									return true;
								}
								if(newStoneIdX == 4 && newStoneIdY == 5)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 5)
							{
								if(newStoneIdX == 3 && newStoneIdY == 5)
								{
									return true;
								}
								if(newStoneIdX == 4 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							break;
						case 5:
							if(gameArray[i][j].getY() == 2)
							{
								if(newStoneIdX == 3 && newStoneIdY == 2)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 4)
							{
								if(newStoneIdX == 6 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 4 && newStoneIdY == 4)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 2)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 6)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 6)
							{
								if(newStoneIdX == 3 && newStoneIdY == 6)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							break;
						case 6:
							if(gameArray[i][j].getY() == 1)
							{
								if(newStoneIdX == 3 && newStoneIdY == 1)
								{
									return true;
								}
								if(newStoneIdX == 6 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 4)
							{
								if(newStoneIdX == 6 && newStoneIdY == 1)
								{
									return true;
								}
								if(newStoneIdX == 6 && newStoneIdY == 7)
								{
									return true;
								}
								if(newStoneIdX == 5 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
							if(gameArray[i][j].getY() == 7)
							{
								if(newStoneIdX == 3 && newStoneIdY == 7)
								{
									return true;
								}
								if(newStoneIdX == 6 && newStoneIdY == 4)
								{
									return true;
								}
								return false;
							}
						}
						
						return false;
					}
				}
			}
		}
		else
		{
			return false;
		}
		return false;
		
	}
	
	
	public boolean checkPhaseC(GridPane gridPaneGame, Phase currentPhase)
	{
		if(currentPhase == Phase.B)
		{
			int counterWhite = 0;
			int counterBlack = 0;
			
			FieldController field = new FieldController(gridPaneGame);
			
			for(int i = 0; i < gridPaneGame.getChildren().size(); i++)
			{
				if(gridPaneGame.getChildren().get(i).getId() != null && gridPaneGame.getChildren().get(i).getId().contains("white"))
				{
					counterWhite += 1;
				}
				else if(gridPaneGame.getChildren().get(i).getId() != null && gridPaneGame.getChildren().get(i).getId().contains("black"))
				{
					counterBlack +=1;
				}
			}
			
			if(counterWhite == 3)
			{
				printMessage(AlertType.INFORMATION, "Phase C", "KI hat nur noch 3 Steine");
				return true;
			}
			if(counterBlack == 3 )
			{
				printMessage(AlertType.INFORMATION, "Phase C", "Spieler 1 hat nur noch 3 Steine");
				return true;
			}
			
			if(counterWhite == 2 || counterWhite == 1)
			{
				printMessage(AlertType.CONFIRMATION, "Spiel beendet", "Spieler 1 hat das Spiel gewonnen. Noch eins?");
				// Starte Spiel neu
				Main.getStage().close();
				Platform.runLater(()-> new Main().start(new Stage()));
			}
			
			if(counterBlack == 2 || counterWhite == 1)
			{
				printMessage(AlertType.INFORMATION, "Spiel beendet", "KI hat das Spiel gewonnen. Noch eins?");
			}
			
		}
		return false;
	}
	
	public void setStone(Dragboard db, Circle circle)
	{	
		circle.setId(db.getString());
		circle.setFill(checkColor(db));
		circle.setOpacity(1);
	}
	
	public boolean checkField(Circle game)
	{
		System.out.println("L�nge: " + game.getId().length());
		System.out.println("ID: " + game.getId());
		
		if(game.getId() != null && game.getId().length()  == 8)
		{
			return true;
		}
		return false;
	}
	
	private Paint checkColor(Dragboard db)
	{
		if(db.getString().contains("black"))
		{
			return Color.BLACK;
		}
		else if(db.getString().contains("white"))
		{
			return Color.WHITE;
		}
		return Color.RED;
	}
	
	public Phase checkPhaseB(Phase currentPhase)
	{
		if(gridPaneWhite.getChildren().size() == 1 && gridPaneBlack.getChildren().size() == 1)
		{
			printMessage(AlertType.INFORMATION, "Phase B", "Alle Wei�en Steine gesetzt");
			//gridPaneGame.getChildren().clear();
			//gridPaneGame.setGridLinesVisible(true);

			return currentPhase.B;
		}
		return currentPhase.A;
	}
	
	// alertType: This Warninigtype you wanted to print
	// title for the Messagetitle
	// text for the Headertext
	public void printMessage(AlertType alertType, String title, String text)
	{
		if(alertType == alertType.INFORMATION)
		{
			Alert alert = new Alert(alertType);
			alert.setTitle(title);
			alert.setHeaderText(text);
			alert.showAndWait();
		}
		else if(alertType == alertType.CONFIRMATION)
		{
			Alert alert = new Alert(alertType);
			alert.setTitle(title);
			alert.setHeaderText(text);
			Optional<ButtonType> result = alert.showAndWait();
			
			if(result.get() == ButtonType.OK)
			{
				// Starte Spiel neu
				Main.getStage().close();
				Platform.runLater(()-> new Main().start(new Stage()));
			}
			else
			{
				System.exit(0);
			}
		}

	}
}
