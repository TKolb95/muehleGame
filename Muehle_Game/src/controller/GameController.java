package controller;
import java.net.URL;
import java.util.ResourceBundle;
import Model.GameRules;
import application.Main;
import container.Gameplay;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/* Autor: Tobias Kolb
 * 3.Semester WI
 *
 */

public class GameController implements Initializable
{
	// Spielfeld Circle 
		@FXML
		private Circle circle01;
	
	    @FXML
	    private Circle circle31;

	    @FXML
	    private Circle circle61;

	    @FXML
	    private Circle circle12;

	    @FXML
	    private Circle circle32;

	    @FXML
	    private Circle circle52;

	    @FXML
	    private Circle circle23;

	    @FXML
	    private Circle circle33;

	    @FXML
	    private Circle circle43;

	    @FXML
	    private Circle circle04;

	    @FXML
	    private Circle circle14;

	    @FXML
	    private Circle circle24;

	    @FXML
	    private Circle circle44;

	    @FXML
	    private Circle circle54;

	    @FXML
	    private Circle circle64;

	    @FXML
	    private Circle circle25;

	    @FXML
	    private Circle circle35;

	    @FXML
	    private Circle circle45;

	    @FXML
	    private Circle circle16;

	    @FXML
	    private Circle circle36;

	    @FXML
	    private Circle circle56;

	    @FXML
	    private Circle circle07;

	    @FXML
	    private Circle circle37;

	    @FXML
	    private Circle circle67;
		
	// --------------------------------
	    
	    
		@FXML
		private GridPane gridPaneGame;
	    

		
	// Schwarze SpielSteine

	    @FXML
	    private Circle blackCircle1;

	    @FXML
	    private Circle blackCircle2;

	    @FXML
	    private Circle blackCircle3;

	    @FXML
	    private Circle blackCircle4;

	    @FXML
	    private Circle blackCircle5;

	    @FXML
	    private Circle blackCircle6;

	    @FXML
	    private Circle blackCircle7;

	    @FXML
	    private Circle blackCircle8;

	    @FXML
	    private Circle blackCircle9;
	// -----------------------------------	
		
		@FXML
		private GridPane gridPaneBlack;


	    
	// Spielsteine Wei�
		
		@FXML
	    private Circle whiteCircle1;

	    @FXML
	    private Circle whiteCircle2;

	    @FXML
	    private Circle whiteCircle3;

	    @FXML
	    private Circle whiteCircle4;

	    @FXML
	    private Circle whiteCircle5;

	    @FXML
	    private Circle whiteCircle6;

	    @FXML
	    private Circle whiteCircle7;

	    @FXML
	    private Circle whiteCircle8;

	    @FXML
	    private Circle whiteCircle9;

		
	// ------------------------------------
	    @FXML
	    private GridPane gridPaneWhite;
	 
	   // MenuItem
	    @FXML
	    private MenuItem restartItem;
	    
	    @FXML
	    private MenuItem closeItem;
	    
	    @FXML
	    private MenuItem aboutItem;
	    
	    

	    public Players playerTurn = Players.PLAYER1;
	    
	    public enum Players { PLAYER1, KI};
	    
	    // A - Steine setzen
	    // B - Spielen
	    // C - Ein Spieler hat nur noch drei Steine
	    public enum Phase {A, B, C };
	    
	    private boolean dragCompleted = false;

	    private  boolean allStonesSet = false;
	    
	    private boolean player1hasMuehle = false;
	    
	    private boolean player2hasMuehle = false;
	    
	    private Phase currentPhase = Phase.A;
	    
	    
	    // KI
	    public KIController KI;

		@Override
		public void initialize(URL location, ResourceBundle ressources)
		{
			
			
			// Erzeuge Spielfeld
			FieldController field = new FieldController(gridPaneBlack,gridPaneWhite, gridPaneGame);
			GameRules gamerules = new GameRules();
			KIController ki = new KIController(gamerules.getGameArray());
			
			
			ObservableList<Circle> blackStones = FXCollections.observableArrayList();
			blackStones.addAll(blackCircle1,blackCircle2,blackCircle3,blackCircle4, blackCircle5, blackCircle6, blackCircle7, blackCircle8, blackCircle9);
			
			ObservableList<Circle> whiteStones = FXCollections.observableArrayList();
			whiteStones.addAll(whiteCircle1, whiteCircle2, whiteCircle3, whiteCircle4, whiteCircle5, whiteCircle6, whiteCircle7, whiteCircle8 , whiteCircle9);
			
			ObservableList<Circle> gameplayStones = FXCollections.observableArrayList();
			gameplayStones.addAll(
					circle01, circle31,circle61, circle12, circle32, circle52, circle23, circle33, circle43, 
					circle04, circle14, circle24, circle44, circle54, 
					circle64, circle25, circle35, circle45, circle16,
					circle36, circle56, circle07, circle37,circle67
					);
			
			
			// blackCircle1 - Spielstein
			for(Circle blackCircle : blackStones) 
			{
				
			blackCircle.setOnDragDetected(new EventHandler<MouseEvent>()
					{

						@Override
						public void handle(MouseEvent event) {
							if(playerTurn == Players.PLAYER1)
							{
								Dragboard db = blackCircle.startDragAndDrop(TransferMode.COPY_OR_MOVE);
								ClipboardContent content = new ClipboardContent();
								content.putString(blackCircle.getId());
								db.setContent(content);
								System.out.println("blackCircle OnDragDetected");
							}
							
							event.consume();
						}
						
				
					});
			blackCircle.setOnDragDone(new EventHandler<DragEvent>()
					{

						@Override
						public void handle(DragEvent event) {
							if(event.getTransferMode() == TransferMode.MOVE && playerTurn == Players.PLAYER1 && currentPhase == Phase.A)
							{
								System.out.println("blackCircle OnDragDone");
								field.remove(gridPaneBlack, event.getDragboard().getString(), gamerules.getGameArray());
								
								changePlayer(playerTurn);								//
								gridPaneGame.requestLayout();
							}
							
							else if(playerTurn == Players.PLAYER1 && currentPhase == Phase.A)
							{
								
								System.out.println("blackCircle OnDragDone ----- transfermode null but why?");
								field.remove(gridPaneBlack, event.getDragboard().getString(), gamerules.getGameArray());

								changePlayer(playerTurn);
							}
							
							if(playerTurn == Players.KI && player1hasMuehle != true && currentPhase == Phase.A)
							{
								
								System.out.println("KI am Zug");
								String oldStone = ki.setStone(gridPaneWhite, gridPaneGame);
								if(oldStone != null)
								{
									changePlayer(playerTurn);
									field.remove(gridPaneWhite, oldStone, gamerules.getGameArray());
		
									if(gamerules.checkMuehle(gridPaneGame, ki.getLastCircle(), ki.getFieldId()) == true)
									{
										ki.removeStoneFromEnemy(gridPaneGame);
									}
									
									System.out.println("Hallo von KI");
									
									// Schalte auf Phase 2, wenn alle Steine gesetzt sind
									currentPhase = field.checkPhaseB(currentPhase);
								}
								else
								{
									// TODO Fehler
									System.out.println("Kein Stein ausgew�hlt!");
								}
							}
						}
				
					});
		}
		
		for(Circle game : gameplayStones)
		{
			// game ist die Variable f�r alle m�glichen Platzierungen f�r die Spielsteine auf dem Spielfeld
			
			game.setOnDragDetected(new EventHandler<MouseEvent>()
					{
						@Override
						public void handle(MouseEvent event) 
						{
							Circle removeStone = (Circle)event.getTarget();
							if(playerTurn == Players.PLAYER1 && (currentPhase == Phase.B || currentPhase == Phase.C) && removeStone.getId().contains("black"))
							{
								System.out.println("Phase 2: Spieler kann Steine legen");
								Dragboard db = game.startDragAndDrop(TransferMode.COPY_OR_MOVE);
								ClipboardContent content = new ClipboardContent();
								content.putString(game.getId());
								db.setContent(content);
								System.out.println("game BlackCircle OnDragDetected");
							}

							event.consume();
						}
				
					});
			
			game.setOnDragDropped(new EventHandler<DragEvent>()
								{
									@Override
									public void handle(DragEvent event)
									{
										Dragboard db = event.getDragboard();
										if(field.checkField(game) == true && currentPhase == Phase.A)
										{
											String stoneField = game.getId();
											field.setStone(db,game);
											event.setDropCompleted(true);
											System.out.println("game OnDragDropped");
											 
											if(gamerules.checkMuehle(gridPaneGame, game, stoneField) == true)
											{
												// es wurde eine M�hle gesetzt
												player1hasMuehle = true;
												field.printMessage(AlertType.INFORMATION, "M�hle", "Spieler 1 hat eine M�hle und darf einen Stein entfernen!");
											}
											
											event.consume();
										}
										else if(field.checkField(game) == true && (currentPhase == Phase.B || currentPhase == Phase.C ) && playerTurn == Players.PLAYER1)
										{
											// Phase 2: Ablegen der Steine auf einen gew�nschten PUnkt
											Gameplay tempGamePlay = field.replaceStone(event, game, gamerules.getGameArray());
										
											if(tempGamePlay != null)
											{
												String tempOldCircleId = "circle"+tempGamePlay.getX()+""+tempGamePlay.getY();
												Circle replaceStone = (Circle) event.getTarget();
												
												if(gamerules.checkMuehle(gridPaneGame, replaceStone, tempOldCircleId) == true)
												{
													field.printMessage(AlertType.INFORMATION, "M�hle", "Spieler 1 hat eine M�hle und darf einen Stein schmei�en");
													player1hasMuehle = true;
												}
											}
											
											// Neuer Stein wurde gesetzt und eine M�hle entsteht
											// gamerules.checkMuehle(gridPaneGame, lastStone, stone);
											changePlayer(playerTurn);
										}
										else
										{
											event.setDropCompleted(false);
											System.out.println("no OnDragDropped");
											event.consume();
										}
									}
									
								});
			game.setOnDragEntered(new EventHandler<DragEvent>()
								{
									@Override
									public void handle(DragEvent event)
									{
										System.out.println("DragEntered");
										System.out.println("game OnDragEntered");
										event.consume();
									}
								});
						
			game.setOnDragOver(new EventHandler<DragEvent>()
								{
									@Override
									public void handle(DragEvent event)
									{
										String temp = game.getId();
								
										if(field.checkField(game) == true && currentPhase == Phase.A)
										{
											System.out.println("game OnDragOver");
											event.acceptTransferModes(TransferMode.MOVE);
										}
										else if(currentPhase == Phase.B && field.checkPlayerTurn(event, temp,gridPaneGame, gamerules.getGameArray()) == true)
										{
											event.acceptTransferModes(TransferMode.MOVE);
										}
										else if(currentPhase == Phase.C && field.checkField(game))
										{
											// Phase C : Spieler darf nun �berall hinsetzen
											event.acceptTransferModes(TransferMode.MOVE);
										}
										else
										{
											System.out.println("game cannot OnDragOver");
											event.acceptTransferModes(TransferMode.NONE);
										}
										
										
										event.consume();
									}
								});
						
			game.setOnDragDone(new EventHandler<DragEvent>()
								{
									@Override
									public void handle(DragEvent event)
									{
										if(event.getTransferMode() == TransferMode.MOVE && currentPhase == Phase.B && playerTurn == Players.KI && player1hasMuehle != true)
										{
											// KI ist nun am Zug
											ki.setStonePhaseB(gridPaneGame, gamerules.getGameArray());
											
											if(gamerules.checkMuehle(gridPaneGame, ki.getLastCircle(), ki.getFieldId()) == true)
											{
												ki.removeStoneFromEnemy(gridPaneGame);
												if(field.checkPhaseC(gridPaneGame, currentPhase) == true)
												{
													currentPhase = Phase.C;
												}
											}
											changePlayer(playerTurn);
										}
										if(currentPhase == Phase.B && playerTurn == Players.KI && player1hasMuehle != true)
										{
											// KI ist nun am Zug in Phase 2, Steine werden hier auf dem Spielfeld erneut platziert
											
											
											// Aufgrund von unerkl�rlichen Gr�nden ist in manchen Spielen der TransferMode immer null
											// daher ist diese Funktion identisch und dient als Backup Funktion
											
											
											ki.setStonePhaseB(gridPaneGame, gamerules.getGameArray());
											
											if(gamerules.checkMuehle(gridPaneGame, ki.getLastCircle(), ki.getFieldId()) == true)
											{
												ki.removeStoneFromEnemy(gridPaneGame);
												if(field.checkPhaseC(gridPaneGame, currentPhase) == true)
												{
													currentPhase = Phase.C;
												}
											}
											changePlayer(playerTurn);
										}
										
										System.out.println("game OnDragDone");
										event.consume();
										// GridPane aktualisieren um eigentlich nicht vorhandene Steine zu l�schen
										gridPaneGame.getParent().requestLayout();
										
									}
								});
			game.setOnDragExited(new EventHandler<DragEvent>()
								{
									@Override
									public void handle(DragEvent event)
									{
										System.out.println("game OnDragExited");
										event.consume();
									}
								});
			// Spieler kann bei M�hle einen Stein des Gegners entfernen
			game.setOnMouseClicked(new EventHandler<MouseEvent>()
					{
						@Override
						public void handle(MouseEvent event) 
						{
							if(player1hasMuehle == true)
							{
								try
								{
									Circle removeStone = (Circle)event.getTarget();
									
									if(removeStone.getId().contains("white"))
									{

										for(int i = 0; i < gamerules.getGameArray().length; i++)

										{
											for(int j = 0; j < gamerules.getGameArray().length; j++)
											{
												Gameplay[][] tempArray = gamerules.getGameArray();
												if((!tempArray[i][j].getColor().contains("invisible")))
												{
													if(tempArray[i][j].getStoneId().equals(removeStone.getId()))
													{
														if(tempArray[i][j].getInMuehle() != true)
														{
															// Stein kann entfernt werden
															field.removeStone(gridPaneGame,removeStone.getId(), gamerules.getGameArray());
															player1hasMuehle = false;
															if(field.checkPhaseC(gridPaneGame, currentPhase) == true)
															{
																currentPhase = Phase.C;
															}
															break;
														}
														else
														{
															field.printMessage(AlertType.WARNING, "Warnung", "Dieser Stein darf nicht entfernt werden!");
															player1hasMuehle = true;
														}
													}
													
												}
											}
										}
									}
									player1hasMuehle = false;
								}
								catch(Exception ex)
								{
									//Fehler es wurde kein Stein angeklickt
									System.out.println(ex);
								}
							}
							if(playerTurn == Players.KI && player1hasMuehle != true && currentPhase == Phase.A)
							{
								System.out.println("KI am Zug");
								String oldStone = ki.setStone(gridPaneWhite, gridPaneGame);
								if(oldStone != null)
								{
									changePlayer(playerTurn);
									field.remove(gridPaneWhite, oldStone, gamerules.getGameArray());
									// oldStone = whiteCircle1
									//
									if(gamerules.checkMuehle(gridPaneGame, ki.getLastCircle(), ki.getFieldId()) == true)
									{
										// KI kann einen Stein entfernen
										ki.removeStoneFromEnemy(gridPaneGame);
									}
									System.out.println("Servus von KI");
								}
								else
								{
									System.out.println("Kein Stein vorhanden!");
								}
								
							}
							if(currentPhase == Phase.B && playerTurn == Players.KI && player1hasMuehle != true)
							{
								// KI ist nun am Zug in Phase 2, Steine werden hier auf dem Spielfeld erneut platziert
								ki.setStonePhaseB(gridPaneGame, gamerules.getGameArray()); 
								if(gamerules.checkMuehle(gridPaneGame, ki.getLastCircle(), ki.getFieldId()) == true)
								{
									ki.removeStoneFromEnemy(gridPaneGame);
								}
								changePlayer(playerTurn);
							}
							if(currentPhase == Phase.C && playerTurn == Players.KI && player1hasMuehle != true)
							{
								// KI ist nun am Zug in Phase 2, Steine werden hier auf dem Spielfeld erneut platziert
								ki.setStonePhaseB(gridPaneGame, gamerules.getGameArray());
								if(gamerules.checkMuehle(gridPaneGame, ki.getLastCircle(), ki.getFieldId()) == true)
								{
									ki.removeStoneFromEnemy(gridPaneGame);
								}
								changePlayer(playerTurn);
							}
						}
				
					});
		}	
			// --------------- Menu Bar ---------------	
			aboutItem.setOnAction(new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent event) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("About");
					alert.setHeaderText("M�hle 1.0");
					alert.setContentText("Autor: Tobias Kolb\nVersion: 1.0\nBugs k�nnen auftreten :D");
					alert.showAndWait();
				}
			});
	
			restartItem.setOnAction(new EventHandler<ActionEvent>()
			{

				@Override
				public void handle(ActionEvent event) {
					Main.getStage().close();
					Platform.runLater(()-> new Main().start(new Stage()) ); 
				}
			});
			
			closeItem.setOnAction(new EventHandler<ActionEvent>()
			{

				@Override
				public void handle(ActionEvent event) 
				{
					System.exit(0);
				}
				
			});
			// --------------- Menu Bar ---------------	
		}
		
		public void changePlayer(Players playerTurn)
		{
			if(playerTurn == Players.KI)
			{
				this.playerTurn = Players.PLAYER1;
			}
			else if(playerTurn == Players.PLAYER1)
			{
				this.playerTurn = Players.KI;
			}
		}
}
