package controller;

import java.util.Random;

import Model.GameRules;
import container.Gameplay;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;


/* Autor: Tobias Kolb
 * 3.Semester WI
 *
 */

public class KIController 
{
	
	private String fieldId;
	
	private Circle lastCircle;
	
	private Gameplay[][] gameArray;
	
	public KIController(Gameplay[][] gameArray)
	{
		setGameArray(gameArray);
	}
	
	public String getFieldId()
	{
		return this.fieldId;
	}
	
	public Circle getLastCircle()
	{
		return this.lastCircle;
	}
	
	private void setGameArray(Gameplay[][] gameArray)
	{
		this.gameArray = gameArray;
	}
	
	// Phase 1
	public String setStone(GridPane gridPaneWhite, GridPane gridPaneGame)
	{
		for(Node n : gridPaneWhite.getChildren())
		{
			if(n != null && n.getId() != null)
			{
				// Suche im Spielfeld einen freien Platz
				boolean success = false;
				String newId = n.getId();
				do
				{
				Random random = new Random();
				int number = random.nextInt(65-41)+41;
				if(gridPaneGame.getChildren().get(number).getId() != null && gridPaneGame.getChildren().get(number).getId().length() == 8)
				{
					System.out.println("Number of KI:" + number);
					this.fieldId = gridPaneGame.getChildren().get(number).getId();
					gridPaneGame.getChildren().get(number).setId(newId);
					gridPaneGame.getChildren().get(number).setOpacity(1);
					//this.lastCircle = (Circle)gridPaneGame.getChildren().get(number);
					Circle tempCircle = (Circle)gridPaneGame.getChildren().get(number);
					tempCircle.setFill(Color.WHITE);
					this.lastCircle = tempCircle;
					success = true;
					
					
					// Im Array �ndern
					int x = Integer.parseInt(getX(this.fieldId));
					int y = Integer.parseInt(getY(this.fieldId));
					gameArray[x][y].setColor("white");
					gameArray[x][y].setInMuehle(false);
					gameArray[x][y].setX(x);
					gameArray[x][y].setY(y);
					gameArray[x][y].setStoneId(newId);
					
					return(gridPaneGame.getChildren().get(number).getId());
				}
				}while(success != true);
				
			}
		}
		return null;
	}
	
	private String getX(String fieldId)
	{
		return ""+fieldId.charAt(6);
	}
	
	private String getY(String fieldId)
	{
		return ""+fieldId.charAt(7);
	}
	
	public void setStonePhaseB(GridPane gridPaneGame, Gameplay[][] gameArray)
	{
		Random random = new Random();
		boolean success = false;
		do 
		{
			int number = random.nextInt(7-0)+0;
			for(int i = 0 ; i < gameArray.length; i++)
			{
				// gameArray[number][i] ist ein Stein, der bewegt werden soll
				if(gameArray[number][i].getColor().equals("white"))
				{
					// possibleTurn liefert X und Y f�r einen m�glichen Zug
					Gameplay possibleTurn = checkLegalTurn(gameArray[number][i], gameArray);
					if(possibleTurn != null)
					{
						String possibleCircle = "circle"+possibleTurn.getX()+""+possibleTurn.getY();
						for(int j = 0; j < gridPaneGame.getChildren().size(); j++)
						{
							if(gridPaneGame.getChildren().get(j).getId() != null && gridPaneGame.getChildren().get(j).getId().equals(possibleCircle))
							{
								//System.out.println("Vorher:");
								//output(gameArray);
								// Neuen Stein auf Spielfeld setzen
								Circle tempCircle = (Circle) gridPaneGame.getChildren().get(j);
								gridPaneGame.getChildren().get(j).setId(gameArray[number][i].getStoneId());
								//gridPaneGame.getChildren().get(j).setOpacity(1);
								

								tempCircle.setFill(Color.WHITE);
								tempCircle.setOpacity(1);
								this.lastCircle = tempCircle;
								this.lastCircle.setFill(Color.WHITE);
								this.lastCircle.setOpacity(1);
								
								// Neuen Stein in Array eintragen
								gameArray[possibleTurn.getX()][possibleTurn.getY()].setColor("white");
								gameArray[possibleTurn.getX()][possibleTurn.getY()].setStoneId(gameArray[number][i].getStoneId());
								gameArray[possibleTurn.getX()][possibleTurn.getY()].setInMuehle(false);
								gameArray[possibleTurn.getX()][possibleTurn.getY()].setX(possibleTurn.getX());
								gameArray[possibleTurn.getX()][possibleTurn.getY()].setY(possibleTurn.getY());
								
								
								// Alten Stein l�schen
								Circle tempArrayCircle = (Circle) gameArray[number][i].getCircle();
								String oldStoneId = "circle"+gameArray[number][i].getX() + "" + gameArray[number][i].getY();
								tempArrayCircle.setOpacity(0);
								tempArrayCircle.setId(oldStoneId);
								tempArrayCircle.setFill(Color.BROWN);
								gameArray[number][i].setStoneId(oldStoneId);
								gameArray[number][i].setInMuehle(false);
								gameArray[number][i].setColor("invisible");
								
								
								
								System.out.println("Nachher:");
								output(gameArray);
								//System.out.println("===========================================================================");
								
								
								// Kontrollausgabe
								System.out.println("Auf FeldId: " + possibleCircle + " wurde der Stein: " + gameArray[number][i].getStoneId() + " gesetzt");
								gameplayControl(gridPaneGame);
								break;
							}
						}
						
						success = true;
						break;
					}
				}
				
			}
		}while(success != true);
		
	}
	// Ein Bug in der GUI.
	// Es wurde versucht die Sichtbarkeit der Steine neu zu setzen
	// Das Problem liegt in der GUI. Die Steine werden optimal im Array ge�ndert allerdings
	// zeigt die GUI manche Spielsteine nicht exakt an, daher kommt es zu dem Problem
	// dass mit zu vielen Steinen gespielt wird.
	// Obwohl wei�e Steine verschoben und daher gel�scht wurden, werden sie dennoch f�r kurze Zeit angezeigt .....
	
	public void gameplayControl(GridPane gridPaneGame)
	{
		for(int i = 0; i < gameArray.length; i++)
		{
			for(int j = 0; j < gameArray.length; j++)
			{
				if(gameArray[i][j].getStoneId() != null && gameArray[i][j].getCircle() != null && gameArray[i][j].getStoneId().contains("circle"))
				{
					Circle tempCircle = (Circle) gameArray[i][j].getCircle();
					tempCircle.setOpacity(0);
				}
				if(gameArray[i][j].getStoneId() != null && gameArray[i][j].getCircle() != null && gameArray[i][j].getStoneId().contains("white"))
				{
					Circle tempCircle = (Circle) gameArray[i][j].getCircle();
					tempCircle.setOpacity(1);
				}
			}
			
		}
	}
	
	public void output(Gameplay[][] gameArray)
	{
		for(int i = 0; i < gameArray.length; i++)
		{
			for(int j = 0; j < gameArray.length; j++)
			{
				System.out.println("StoneID:" + gameArray[i][j].getStoneId());
				System.out.println("Color : " + gameArray[i][j].getColor());
				System.out.println("X:" + gameArray[i][j].getX());
				System.out.println("Y:" + gameArray[i][j].getY());
				System.out.println("-------------------------------------------");
			}
		}
	}
	
	
	public void removeStoneFromEnemy(GridPane gridPaneGame)
	{
		Random random = new Random();
		boolean success = false;
		do
		{
			int number = random.nextInt(64-41)+41;
			
			if(gridPaneGame.getChildren().get(number) != null && gridPaneGame.getChildren().get(number).getId() != null && gridPaneGame.getChildren().get(number).getId().contains("black"))
			{
				for(int i = 0 ; i < gameArray.length; i++)
				{
					for(int j = 0; j < gameArray.length; j++)
					{
						if(gridPaneGame.getChildren().get(number).getId().equals(gameArray[i][j].getStoneId()))
						{
							if(gameArray[i][j].getInMuehle() != true)
							{
								success = true;
								// Stein kann entfernt werden
								FieldController field = new FieldController(gridPaneGame);
								field.removeStone(gridPaneGame, gameArray[i][j].getStoneId(), gameArray);
							}
						}
					}
				}
			}
			
		}while(success != true);
	}
	
	
	
	public Gameplay checkLegalTurn(Gameplay gameArrayStone, Gameplay[][] gameArray)
	{
		
		if(gameArrayStone.getStoneId() != null)
		{
			switch(gameArrayStone.getX())
			{
			case 0:
				if(gameArrayStone.getY() == 1)
				{
					// 04 m�glich , 31 m�glich
					if(gameArray[0][4].getStoneId() != null && gameArray[0][4].getColor().equals("invisible"))
					{
						gameArray[0][4].setX(0);
						gameArray[0][4].setY(4);
						return gameArray[0][4];
					}
					else if(gameArray[3][1].getStoneId() != null && gameArray[3][1].getColor().equals("invisible"))
					{
						gameArray[3][1].setX(3);
						gameArray[3][1].setY(1);
						return gameArray[3][1];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 4)
				{
					if(gameArray[0][1].getStoneId() != null && gameArray[0][1].getColor().equals("invisible"))
					{
						gameArray[0][1].setX(0);
						gameArray[0][1].setY(1);
						return gameArray[0][1];
					}
					else if(gameArray[0][7].getStoneId() != null && gameArray[0][7].getColor().equals("invisible"))
					{
						gameArray[0][7].setX(0);
						gameArray[0][7].setY(7);
						return gameArray[0][7];
					}
					else if(gameArray[1][4].getStoneId() != null && gameArray[1][4].getColor().equals("invisible"))
					{
						gameArray[1][4].setX(1);
						gameArray[1][4].setY(4);
						return gameArray[1][4];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 7)
				{
					if(gameArray[0][4].getStoneId() != null && gameArray[0][4].getColor().equals("invisible"))
					{
						gameArray[0][4].setX(0);
						gameArray[0][4].setY(4);
						return gameArray[0][4];
					}
					else if(gameArray[3][7].getStoneId() != null && gameArray[3][7].getColor().equals("invisible"))
					{
						gameArray[3][7].setX(3);
						gameArray[3][7].setY(7);
						return gameArray[3][7];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
			break;
			case 1:
				if(gameArrayStone.getY() == 2)
				{
					if(gameArray[3][2].getStoneId() != null && gameArray[3][2].getColor().equals("invisible"))
					{
						gameArray[3][2].setX(3);
						gameArray[3][2].setY(2);
						return gameArray[3][2];
					}
					else if(gameArray[1][4].getStoneId() != null && gameArray[1][4].getColor().equals("invisible"))
					{
						gameArray[1][4].setX(1);
						gameArray[1][4].setY(4);
						return gameArray[1][4];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 4)
				{
					if(gameArray[0][4].getStoneId() != null && gameArray[0][4].getColor().equals("invisible"))
					{
						gameArray[0][4].setX(0);
						gameArray[0][4].setY(4);
						return gameArray[0][4];
					}
					else if(gameArray[2][4].getStoneId() != null && gameArray[2][4].getColor().equals("invisible"))
					{
						gameArray[2][4].setX(2);
						gameArray[2][4].setY(4);
						return gameArray[2][4];
					}
					else if(gameArray[1][6].getStoneId() != null && gameArray[1][6].getColor().equals("invisible"))
					{
						gameArray[1][6].setX(1);
						gameArray[1][6].setY(6);
						return gameArray[1][6];
					}
					else if(gameArray[1][2].getStoneId() != null && gameArray[1][2].getColor().equals("invisible"))
					{
						gameArray[1][2].setX(1);
						gameArray[1][2].setY(2);
						return gameArray[1][2];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
			break;
			case 2:
				if(gameArrayStone.getY() == 3)
				{
					if(gameArray[3][3].getStoneId() != null && gameArray[3][3].getColor().equals("invisible"))
					{
						gameArray[3][3].setX(3);
						gameArray[3][3].setY(3);
						return gameArray[3][3];
					}
					else if(gameArray[2][4].getStoneId() != null && gameArray[2][4].getColor().equals("invisible"))
					{
						gameArray[2][4].setX(2);
						gameArray[2][4].setY(4);
						return gameArray[2][4];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 4)
				{
					if(gameArray[1][4].getStoneId() != null && gameArray[1][4].getColor().equals("invisible"))
					{
						gameArray[1][4].setX(1);
						gameArray[1][4].setY(4);
						return gameArray[1][4];
					}
					else if(gameArray[2][3].getStoneId() != null && gameArray[2][3].getColor().equals("invisible"))
					{
						gameArray[2][3].setX(2);
						gameArray[2][3].setY(3);
						return gameArray[2][3];
					}
					else if(gameArray[2][5].getStoneId() != null && gameArray[2][5].getColor().equals("invisible"))
					{
						gameArray[2][5].setX(2);
						gameArray[2][5].setY(5);
						return gameArray[2][5];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 5)
				{
					if(gameArray[3][5].getStoneId() != null && gameArray[3][5].getColor().equals("invisible"))
					{
						gameArray[3][5].setX(3);
						gameArray[3][5].setY(5);
						return gameArray[3][5];
					}
					else if(gameArray[2][4].getStoneId() != null && gameArray[2][4].getColor().equals("invisible"))
					{
						gameArray[2][4].setX(2);
						gameArray[2][4].setY(4);
						return gameArray[2][4];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
			break;
			case 3:
				if(gameArrayStone.getY() == 1)
				{
					if(gameArray[0][1].getStoneId() != null && gameArray[0][1].getColor().equals("invisible"))
					{
						gameArray[0][1].setX(0);
						gameArray[0][1].setY(1);
						return gameArray[0][1];
					}
					else if(gameArray[6][1].getStoneId() != null && gameArray[6][1].getColor().equals("invisible"))
					{
						gameArray[6][1].setX(6);
						gameArray[6][1].setY(1);
						return gameArray[6][1];
					}
					else if(gameArray[3][2].getStoneId() != null && gameArray[3][2].getColor().equals("invisible"))
					{
						gameArray[3][2].setX(3);
						gameArray[3][2].setY(2);
						return gameArray[3][2];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 2)
				{
					if(gameArray[1][2].getStoneId() != null && gameArray[1][2].getColor().equals("invisible"))
					{
						gameArray[1][2].setX(1);
						gameArray[1][2].setY(2);
						return gameArray[1][2];
					}
					else if(gameArray[5][2].getStoneId() != null && gameArray[5][2].getColor().equals("invisible"))
					{
						gameArray[5][2].setX(5);
						gameArray[5][2].setY(2);
						return gameArray[5][2];
					}
					else if(gameArray[3][1].getStoneId() != null && gameArray[3][1].getColor().equals("invisible"))
					{
						gameArray[3][1].setX(3);
						gameArray[3][1].setY(1);
						return gameArray[3][1];
					}
					else if(gameArray[3][3].getStoneId() != null && gameArray[3][3].getColor().equals("invisible"))
					{
						gameArray[3][3].setX(3);
						gameArray[3][3].setY(3);
						return gameArray[3][3];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 3)
				{
					if(gameArray[2][3].getStoneId() != null && gameArray[2][3].getColor().equals("invisible"))
					{
						gameArray[2][3].setX(2);
						gameArray[2][3].setY(3);
						return gameArray[2][3];
					}
					else if(gameArray[4][3].getStoneId() != null && gameArray[4][3].getColor().equals("invisible"))
					{
						gameArray[4][3].setX(4);
						gameArray[4][3].setY(3);
						return gameArray[4][3];
					}
					else if(gameArray[3][2].getStoneId() != null && gameArray[3][2].getColor().equals("invisible"))
					{
						gameArray[3][2].setX(3);
						gameArray[3][2].setY(2);
						return gameArray[3][2];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 5)
				{
					if(gameArray[2][5].getStoneId() != null && gameArray[2][5].getColor().equals("invisible"))
					{
						gameArray[2][5].setX(2);
						gameArray[2][5].setY(5);
						return gameArray[2][5];
					}
					else if(gameArray[4][5].getStoneId() != null && gameArray[4][5].getColor().equals("invisible"))
					{
						gameArray[4][5].setX(4);
						gameArray[4][5].setY(5);
						return gameArray[4][5];
					}
					else if(gameArray[3][6].getStoneId() != null && gameArray[3][6].getColor().equals("invisible"))
					{
						gameArray[3][6].setX(3);
						gameArray[3][6].setY(6);
						return gameArray[3][6];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 6)
				{
					if(gameArray[1][6].getStoneId() != null && gameArray[1][6].getColor().equals("invisible"))
					{
						gameArray[1][6].setX(1);
						gameArray[1][6].setY(6);
						return gameArray[1][6];
					}
					else if(gameArray[5][6].getStoneId() != null && gameArray[5][6].getColor().equals("invisible"))
					{
						gameArray[5][6].setX(5);
						gameArray[5][6].setY(6);
						return gameArray[5][6];
					}
					else if(gameArray[3][5].getStoneId() != null && gameArray[3][5].getColor().equals("invisible"))
					{
						gameArray[3][5].setX(3);
						gameArray[3][5].setY(5);
						return gameArray[3][5];
					}
					else if(gameArray[3][7].getStoneId() != null && gameArray[3][7].getColor().equals("invisible"))
					{
						gameArray[3][7].setX(3);
						gameArray[3][7].setY(7);
						return gameArray[3][7];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 7)
				{
					if(gameArray[0][7].getStoneId() != null && gameArray[0][7].getColor().equals("invisible"))
					{
						gameArray[0][7].setX(0);
						gameArray[0][7].setY(7);
						return gameArray[0][7];
					}
					else if(gameArray[6][7].getStoneId() != null && gameArray[6][7].getColor().equals("invisible"))
					{
						gameArray[6][7].setX(6);
						gameArray[6][7].setY(7);
						return gameArray[6][7];
					}
					else if(gameArray[3][6].getStoneId() != null && gameArray[3][6].getColor().equals("invisible"))
					{
						gameArray[3][6].setX(3);
						gameArray[3][6].setY(6);
						return gameArray[3][6];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				break;
			case 4:
				if(gameArrayStone.getY() == 3)
				{
					if(gameArray[4][4].getStoneId() != null && gameArray[4][4].getColor().equals("invisible"))
					{
						gameArray[4][4].setX(4);
						gameArray[4][4].setY(4);
						return gameArray[4][4];
					}
					else if(gameArray[3][3].getStoneId() != null && gameArray[3][3].getColor().equals("invisible"))
					{
						gameArray[3][3].setX(3);
						gameArray[3][3].setY(3);
						return gameArray[3][3];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 4)
				{
					if(gameArray[4][3].getStoneId() != null && gameArray[4][3].getColor().equals("invisible"))
					{
						gameArray[4][3].setX(4);
						gameArray[4][3].setY(3);
						return gameArray[4][3];
					}
					else if(gameArray[4][5].getStoneId() != null && gameArray[4][5].getColor().equals("invisible"))
					{
						gameArray[4][5].setX(4);
						gameArray[4][5].setY(5);
						return gameArray[4][5];
					}
					else if(gameArray[5][4].getStoneId() != null && gameArray[5][4].getColor().equals("invisible"))
					{
						gameArray[5][4].setX(5);
						gameArray[5][4].setY(4);
						return gameArray[5][4];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 5)
				{
					if(gameArray[3][5].getStoneId() != null && gameArray[3][5].getColor().equals("invisible"))
					{
						gameArray[3][5].setX(3);
						gameArray[3][5].setY(5);
						return gameArray[3][5];
					}
					else if(gameArray[4][4].getStoneId() != null && gameArray[4][4].getColor().equals("invisible"))
					{
						gameArray[4][4].setX(4);
						gameArray[4][4].setY(4);
						return gameArray[4][4];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
			break;
			case 5:
				if(gameArrayStone.getY() == 2)
				{
					if(gameArray[5][4].getStoneId() != null && gameArray[5][4].getColor().equals("invisible"))
					{
						gameArray[5][4].setX(5);
						gameArray[5][4].setY(4);
						return gameArray[5][4];
					}
					else if(gameArray[3][2].getStoneId() != null && gameArray[3][2].getColor().equals("invisible"))
					{
						gameArray[3][2].setX(3);
						gameArray[3][2].setY(2);
						return gameArray[3][2];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 4)
				{
					if(gameArray[6][4].getStoneId() != null && gameArray[6][4].getColor().equals("invisible"))
					{
						gameArray[6][4].setX(6);
						gameArray[6][4].setY(4);
						return gameArray[6][4];
					}
					else if(gameArray[4][4].getStoneId() != null && gameArray[4][4].getColor().equals("invisible"))
					{
						gameArray[4][4].setX(4);
						gameArray[4][4].setY(4);
						return gameArray[4][4];
					}
					else if(gameArray[5][2].getStoneId() != null && gameArray[5][2].getColor().equals("invisible"))
					{
						gameArray[5][2].setX(5);
						gameArray[5][2].setY(2);
						return gameArray[5][2];
					}
					else if(gameArray[5][6].getStoneId() != null && gameArray[5][6].getColor().equals("invisible"))
					{
						gameArray[5][6].setX(5);
						gameArray[5][6].setY(6);
						return gameArray[5][6];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 6)
				{
					if(gameArray[5][4].getStoneId() != null && gameArray[5][4].getColor().equals("invisible"))
					{
						gameArray[5][4].setX(5);
						gameArray[5][4].setY(4);
						return gameArray[5][4];
					}
					else if(gameArray[3][6].getStoneId() != null && gameArray[3][6].getColor().equals("invisible"))
					{
						gameArray[3][6].setX(3);
						gameArray[3][6].setY(6);
						return gameArray[3][6];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				break;
			case 6:
				if(gameArrayStone.getY() == 1)
				{
					if(gameArray[6][4].getStoneId() != null && gameArray[6][4].getColor().equals("invisible"))
					{
						gameArray[6][4].setX(6);
						gameArray[6][4].setY(4);
						return gameArray[6][4];
					}
					else if(gameArray[3][1].getStoneId() != null && gameArray[3][1].getColor().equals("invisible"))
					{
						gameArray[3][1].setX(3);
						gameArray[3][1].setY(1);
						return gameArray[3][1];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 4)
				{
					if(gameArray[5][4].getStoneId() != null && gameArray[5][4].getColor().equals("invisible"))
					{
						gameArray[5][4].setX(5);
						gameArray[5][4].setY(4);
						return gameArray[5][4];
					}
					else if(gameArray[6][1].getStoneId() != null && gameArray[6][1].getColor().equals("invisible"))
					{
						gameArray[6][1].setX(6);
						gameArray[6][1].setY(1);
						return gameArray[6][1];
					}
					else if(gameArray[6][7].getStoneId() != null && gameArray[6][7].getColor().equals("invisible"))
					{
						gameArray[6][7].setX(6);
						gameArray[6][7].setY(7);
						return gameArray[6][7];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				if(gameArrayStone.getY() == 7)
				{
					if(gameArray[6][4].getStoneId() != null && gameArray[6][4].getColor().equals("invisible"))
					{
						gameArray[6][4].setX(6);
						gameArray[6][4].setY(4);
						return gameArray[6][4];
					}
					else if(gameArray[3][7].getStoneId() != null && gameArray[3][7].getColor().equals("invisible"))
					{
						gameArray[3][7].setX(3);
						gameArray[3][7].setY(7);
						return gameArray[3][7];
					}
					else
					{
						// Kein Zug hier m�glich
						return null;
					}
				}
				break;
			}
		}
		
		return null;
	}
	
	
}
